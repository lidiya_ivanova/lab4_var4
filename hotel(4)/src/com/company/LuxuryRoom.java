package com.company;

public class LuxuryRoom extends Room{
    private String min_deadline;
    private String max_deadline;

    public LuxuryRoom (int id, int number, int count, int price, String min_deadline,String max_deadline) {
        super (id,number,count,price);
        this.min_deadline = min_deadline;
        this.max_deadline = max_deadline;
    }


    @Override
    public String toString() {
        return super.toString()+"\n"+
                "min_deadline=" + min_deadline + "\n" +
                "max_deadline=" + max_deadline;
    }
}

