package com.company;

public class StandardRoom extends Room{
    private int id;
    private int number;
    private int count;
    private int price;

    public StandardRoom (int id, int number, int count, int price) {
        super (id,number,count,price);
            this.id = id;
            this.number = number;
            this.count = count;
            this.price = price;
        }
    @Override
    public String toString() {
        return super.toString()+"\n"+
                "id=" + id + "\n" +
                "number=" + number + "\n" +
                "count=" + count + "\n" +
                "price=" + price;
        }
    }

