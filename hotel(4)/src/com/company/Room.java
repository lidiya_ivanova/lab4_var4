package com.company;

public class Room {
    private int id;
    private int number;
    private int count;
    private int price;

    public Room (int id, int number, int count, int price) {
        this.id = id;
        this.number = number;
        this.count = count;
        this.price = price;
    }
    @Override
    public String toString() {
        return super.toString()+"\n"+
                "id=" + id + "\n" +
                "number=" + number + "\n" +
                "count=" + count + "\n" +
                "price=" + price;
    }
}



