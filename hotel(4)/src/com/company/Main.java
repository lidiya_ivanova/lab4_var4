package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList <Room> commodityArrayList = new ArrayList<>();

        commodityArrayList.add(new StandardRoom(123, 5, 2, 2000));

        commodityArrayList.add(new PoluLuxuryRoom(125,6,4,4000));

        commodityArrayList.add(new LuxuryRoom(123,7,5,5000,"11.12.2020", "15.12.2020"));

        for(Room com: commodityArrayList){
            System.out.println(com.toString());
        }
    }


}
